package com.cloudsquareinc.praveenkumar.backend.data;

public class Role {
	public static final String BAKER = "baker";
	public static final String ADMIN = "admin";

	private Role() {
		// Static methods and fields only
	}

	public static String[] getAllRoles() {
		return new String[] { BAKER, ADMIN };
	}

}
