package com.cloudsquareinc.praveenkumar.backend;

import com.cloudsquareinc.praveenkumar.backend.data.entity.User;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {

	User findByUsername(String username);

	Page<User> findByUsernameLikeIgnoreCaseOrNameLikeIgnoreCaseOrRoleLikeIgnoreCase(String usernameLike, String nameLike,
                                                                                  String roleLike, Pageable pageable);

	long countByUsernameLikeIgnoreCaseOrNameLikeIgnoreCase(String usernameLike, String nameLike);
}
