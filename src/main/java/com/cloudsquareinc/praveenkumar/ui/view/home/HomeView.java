package com.cloudsquareinc.praveenkumar.ui.view.home;

import com.vaadin.navigator.View;
import com.vaadin.spring.annotation.SpringView;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by nedzad on 9/14/17.
 */
@SpringView
public class HomeView implements View {

  private final HomeViewDesign homeViewDesign;

  @Autowired
  public HomeView() {
    homeViewDesign = new HomeViewDesign();
  }

  @Override
  public HomeViewDesign getViewComponent() {
    return homeViewDesign;
  }
}
