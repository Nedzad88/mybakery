package com.cloudsquareinc.praveenkumar.app;

import com.cloudsquareinc.praveenkumar.backend.UserRepository;
import com.cloudsquareinc.praveenkumar.backend.data.Role;
import com.cloudsquareinc.praveenkumar.backend.data.entity.User;
import com.vaadin.spring.annotation.SpringComponent;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.password.PasswordEncoder;

@SpringComponent
public class DataGenerator implements HasLogger {

	@Bean
	public CommandLineRunner loadData(UserRepository userRepository, PasswordEncoder passwordEncoder) {
		return args -> {
			if (hasData(userRepository)) {
				getLogger().info("Using existing database");
				return;
			}

			getLogger().info("Generating demo data");
			getLogger().info("... generating users");
			createUsers(userRepository, passwordEncoder);
			getLogger().info("Generated demo data");
		};
	}

	private boolean hasData(UserRepository userRepository) {
		return userRepository.count() != 0L;
	}

	private void createUsers(UserRepository userRepository, PasswordEncoder passwordEncoder) {
		User admin = new User("admin", "Göran", passwordEncoder.encode("admin"), Role.ADMIN);
    admin.setLocked(true);
		userRepository.save(admin);
	}
}
